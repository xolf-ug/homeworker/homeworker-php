# Einführung
Der Client für die Homeworker-Api befindet sich noch in der Alpha-Entwicklung.  
Es können möglicherweiße noch Major-Changes vorgenommen werden.

# Einrichtung
Zur Verwendung der meisten Api-Anwendungen ist ein Api-Token notwendig, welche in [Hubi generiert](https://homeworker.li/hubi/api/app/add) werden können. 

```
composer require homeworker/homeworker-php
```


```php
Homeworker\Api\Client::setApiToken('<TOKEN>');
```

# Verwendung

## Resourcen erhalten

### Ohne ID
```php
/** @var Homeworker\Api\Resources\SchoolMananger\School $school **/
$school = Homeworker\Api\Resources\SchoolMananger\School::retrieve();
echo $school->name; // "Beispiel-Gymnasium Musterhausen"
```

### Mit ID
```php
/** @var Homeworker\Api\Resources\SchoolMananger\Students $student **/
$student = Homeworker\Api\Resources\SchoolMananger\Students::retrieve('<ID>');
echo $student->first_name; // "Max"
```

## Resource erstellen
```php
/** @var Homeworker\Api\Resources\SchoolMananger\Students $student **/
$student = Homeworker\Api\Resources\SchoolMananger\Students::create([
    'first_name' => 'Marion', 
    'last_name' => 'Musterfrau', 
    'birthday' => '2002-11-23',
    'grade' => '9',
    'course' => '9c',
    'import_key' => 'key-123456',
    'import_source' => 'api'
]);
echo $student->id; // z.B. 532636
echo $student->first_name; // z.B. "Marion"
```

## Resource updaten
```php
/** @var Homeworker\Api\Resources\SchoolMananger\Students $student **/
$student = Homeworker\Api\Resources\SchoolMananger\Students::update($student_id, [
    'first_name' => 'Marionetta', 
]);
echo $student->first_name; // z.B. "Marionetta"
```