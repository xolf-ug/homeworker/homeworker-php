<?php

namespace Homeworker\Api\Traits;

trait SetStatableTrait
{

    /**
     * @param array $properties
     * @return static
     */
    public static function __set_state(array $properties)
    {
        $resourceObject = new static();

        foreach ($properties as $property => $value) {
            $resourceObject->$property = $value;
        }

        return $resourceObject;
    }

}
