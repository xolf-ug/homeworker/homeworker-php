<?php

namespace Homeworker\Api\Traits;

trait ArraybleTrait
{

    /**
     * Returns object as array
     *
     * @return array
     */
    public function toArray(): array
    {
        $data = get_object_vars($this);

        foreach ($data as $name => $valie) {
            if(is_array($data[$name])) {
                $toArray = [];
                foreach ($data[$name] as $datum) {
                    if(is_object($datum)) {
                        if(!method_exists($datum, 'toArray')) {
                            throw new \InvalidArgumentException(get_class($datum).": No array can be generated, because elements of array must be a ArrayableTrait or have an toArray method");
                        }

                        $toArray[] = $datum->toArray();
                    } else {
                        $toArray[] = $datum;
                    }
                }

                $data[$name] = $toArray;
            }
        }

        return $data;
    }

}
