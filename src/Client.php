<?php

namespace Homeworker\Api;

use Homeworker\Api\Exceptions\MissingApiTokenException;

class Client
{

    /** @var string */
    private static $apiToken;

    /** @var string */
    private static $apiBase = 'https://homeworker.li/api';

    /** @var string */
    private static $apiVersion = 'v2';

    /** @var null|string */
    private static $userAgentExtension;

    /** @var array  */
    private static $headers = [];

    /** @var \GuzzleHttp\Client */
    private static $guzzleClient;

    /** @var Requestor */
    private static $requestor;

    /** @var string */
    const VERSION = '0.0.35';

    /**
     * @return string
     */
    public static function getApiToken(): string
    {
        return self::$apiToken;
    }

    /**
     * @param string $apiToken
     */
    public static function setApiToken(string $apiToken)
    {
        self::$apiToken = $apiToken;
    }

    /**
     * @return string
     */
    public static function getApiBase(): string
    {
        return self::$apiBase;
    }

    /**
     * @param string $apiBase
     */
    public static function setApiBase(string $apiBase)
    {
        self::$apiBase = $apiBase;
    }

    /**
     * @return string
     */
    public static function getApiVersion(): string
    {
        return self::$apiVersion;
    }

    /**
     * @param string $apiVersion
     */
    public static function setApiVersion(string $apiVersion)
    {
        self::$apiVersion = $apiVersion;
    }

    /**
     * @return null|string
     */
    public static function getUserAgentExtension(): ?string
    {
        return self::$userAgentExtension;
    }

    /**
     * @param null|string $userAgentExtension
     */
    public static function setUserAgentExtension(?string $userAgentExtension): void
    {
        self::$userAgentExtension = $userAgentExtension;
    }

    /**
     * @return array
     */
    public static function getHeaders(): array
    {
        return self::$headers;
    }

    /**
     * @param array $headers
     */
    public static function setHeaders(array $headers): void
    {
        self::$headers = $headers;
    }

    /**
     * @return string
     */
    public static function getFullApiBaseUri(): string
    {
        return self::getApiBase().'/'.self::getApiVersion().'/';
    }

    /**
     * @return \GuzzleHttp\Client
     * @throws MissingApiTokenException#
     */
    public static function getGuzzleClient(): \GuzzleHttp\Client
    {
        if(is_null(self::$guzzleClient)) {
            if(is_null(self::$apiToken)) {
                throw new MissingApiTokenException("No Api-Token is provided. Please set Api-Token via Homeworker\\Api\\Client::setApiToken('<API_TOKEN>')");
            }

            $userAgent = 'homeworker-php v'.self::VERSION;
            if(self::$userAgentExtension) $userAgent = self::$userAgentExtension.'/'.$userAgent;

            $headers = array_merge(self::$headers, [
                'Authorization' => 'Bearer '.self::getApiToken(),
                'User-Agent' => $userAgent,
                'X-Client' => 'homeworker-php '.self::VERSION
            ]);

            self::$guzzleClient = new \GuzzleHttp\Client([
                'base_uri' => self::getFullApiBaseUri(),
                'headers' => $headers,
                'timeout' => 10
            ]);
        }

        return self::$guzzleClient;
    }

    /**
     * @param \GuzzleHttp\Client $guzzleClient
     */
    public static function setGuzzleClient(\GuzzleHttp\Client $guzzleClient)
    {
        self::$guzzleClient = $guzzleClient;
    }

    /**
     * @return Requestor
     * @throws MissingApiTokenException
     */
    public static function getRequestor(): Requestor
    {
        if(is_null(self::$requestor)) {
            self::$requestor = new Requestor(self::getGuzzleClient());
        }

        return self::$requestor;
    }

    /**
     * @param Requestor $requestor
     */
    public static function setRequestor(Requestor $requestor)
    {
        self::$requestor = $requestor;
    }

}
