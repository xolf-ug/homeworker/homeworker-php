<?php

namespace Homeworker\Api;

use GuzzleHttp\RequestOptions;
use Homeworker\Api\Exceptions\InvalidApiTokenException;
use Homeworker\Api\Exceptions\NotFoundException;
use Homeworker\Api\Exceptions\RequestException;
use Homeworker\Api\Exceptions\ResponseException;
use Homeworker\Api\Exceptions\ApiError;
use Homeworker\Api\Resources\AbstractResource;
use Psr\Http\Message\ResponseInterface;

class Requestor
{

    /** @var \GuzzleHttp\Client */
    private $guzzle;

    public function __construct(?\GuzzleHttp\Client $guzzle = null)
    {
        if(!is_null($guzzle)) $this->guzzle = $guzzle;
    }

    /**
     * @return \GuzzleHttp\Client
     */
    public function getGuzzle(): \GuzzleHttp\Client
    {
        return $this->guzzle;
    }

    /**
     * @param \GuzzleHttp\Client $guzzle
     */
    public function setGuzzle(\GuzzleHttp\Client $guzzle): void
    {
        $this->guzzle = $guzzle;
    }

    /**
     * @param string $method
     * @param AbstractResource|string $resource
     * @param int|null $resourceId
     * @param array $params
     * @return AbstractResource
     * @throws InvalidApiTokenException
     * @throws RequestException
     * @throws ResponseException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function send(string $method, $resource, ?int $resourceId = null, array $params = []): AbstractResource
    {
        if(is_string($resource)) {
            $resource = new $resource();
        }
        /** @var AbstractResource $resource */
        $uri = $resource::URI;

        // Append Resource ID to URL when given
        if($resourceId) {
            $uri .= '/'.$resourceId;
        }

        $response = $this->sendPlain($method, $uri, $params);

        $jsonResponse = json_decode($response->getBody(), true);

        /** @var AbstractResource $instance */
        $instance = $resource::__set_state($jsonResponse);

        return $instance;
    }

    /**
     * Send Plain HTTP-Request to Api
     *
     * @param string $method
     * @param string $uri
     * @param array $params
     * @param array $options
     * @return ResponseInterface
     * @throws InvalidApiTokenException
     * @throws RequestException
     * @throws ResponseException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendPlain(string $method, string $uri, array $params = [], array $options = []): ResponseInterface
    {
        if(strtolower($method) == 'get' && $params != []) {
            $uri .= '?'.http_build_query($params);
        }
        if(strtolower($method) == 'post' && $params != []) {
            $options[RequestOptions::JSON] = $params;
        }

        try {
            $response = $this->guzzle->request($method, $uri, $options);

            return $response;
        } catch (\GuzzleHttp\Exception\ClientException $exception) {
            $responseBody = (string)$exception->getResponse()->getBody();
            $errorJson = json_decode($responseBody, true);

            if($exception->getResponse()->getStatusCode() == 401 && $errorJson['error']['name'] == 'NoValidApiToken') {
                throw new InvalidApiTokenException("API token is not valid");
            } else if($exception->getResponse()->getStatusCode() == 404) {
                throw new NotFoundException($errorJson['error']['message']);
            } else {
                throw new ApiError($errorJson['error']['message'], $errorJson['error']['code']);
            }
        } catch (\Exception $e) {
            throw new RequestException(get_class($e).": ".$e->getMessage(), $e->getCode());
        }
    }

}
