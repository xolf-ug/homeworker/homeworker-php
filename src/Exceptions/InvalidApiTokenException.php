<?php

namespace Homeworker\Api\Exceptions;

class InvalidApiTokenException extends RequestException {}
