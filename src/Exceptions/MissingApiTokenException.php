<?php

namespace Homeworker\Api\Exceptions;

class MissingApiTokenException extends \Exception {}
