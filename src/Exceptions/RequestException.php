<?php

namespace Homeworker\Api\Exceptions;

class RequestException extends HomeworkerException {}
