<?php

namespace Homeworker\Api\Exceptions;

class NotFoundException extends RequestException {}
