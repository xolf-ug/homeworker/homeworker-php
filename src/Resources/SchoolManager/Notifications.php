<?php

namespace Homeworker\Api\Resources\SchoolManager;

use Homeworker\Api\Client;
use Homeworker\Api\Exceptions\ApiError;
use Homeworker\Api\Resources\AbstractResource;
use Homeworker\Api\ResourceTraits\CollectionResource;
use Homeworker\Api\ResourceTraits\DeletableResource;
use Homeworker\Api\ResourceTraits\EditableResource;
use Homeworker\Api\ResourceTraits\RetrievableResource;

class Notifications extends AbstractResource
{
    use RetrievableResource;
    use CollectionResource;
    use EditableResource;
    use DeletableResource;

    const URI = 'school_manager/notifications';

    const STATE_SCHEDULED = 'scheduled';
    const STATE_PUBLISHED = 'published';
    const STATE_EXPIRED = 'expired';

    /** @var int */
    public $id;

    /** @var string */
    public $title;

    /** @var string */
    public $body;

    /** @var null|string */
    public $subtext;

    /** @var null|string */
    public $link;

    /** @var null|string */
    public $image;

    /** @var int */
    public $pinned;

    /** @var int */
    public $notify;

    /** @var string */
    public $visible_from;

    /** @var string */
    public $visible_to;

    /** @var int */
    public $to_all;

    /** @var string */
    public $state;

    /** @var null|string */
    public $import_source;

    /** @var null|string */
    public $import_key;

    /** @var array */
    public $documents = [];


    /**
     * Gibt alle verbundene student_ids der Mitteilung zurück
     *
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Homeworker\Api\Exceptions\InvalidApiTokenException
     * @throws \Homeworker\Api\Exceptions\MissingApiTokenException
     * @throws \Homeworker\Api\Exceptions\RequestException
     * @throws \Homeworker\Api\Exceptions\ResponseException
     */
    public function student_ids(): array
    {
        if(!$this->id) return [];

        $response = Client::getRequestor()->sendPlain('GET', self::URI.'/'.$this->id.'/student_ids', []);

        return json_decode($response->getBody());
    }

    /**
     * Gibt alle verbundene group_ids der Mitteilung zurück
     *
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Homeworker\Api\Exceptions\InvalidApiTokenException
     * @throws \Homeworker\Api\Exceptions\MissingApiTokenException
     * @throws \Homeworker\Api\Exceptions\RequestException
     * @throws \Homeworker\Api\Exceptions\ResponseException
     */
    public function group_ids(): array
    {
        if(!$this->id) return [];

        $response = Client::getRequestor()->sendPlain('GET', self::URI.'/'.$this->id.'/group_ids', []);

        return json_decode($response->getBody());
    }

}
