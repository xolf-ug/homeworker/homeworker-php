<?php

namespace Homeworker\Api\Resources\SchoolManager;

use Homeworker\Api\Resources\AbstractResource;
use Homeworker\Api\Resources\Exceptions\InvalidMethodException;
use Homeworker\Api\ResourceTraits\EditableResource;

class Message extends AbstractResource
{
    use EditableResource;

    const URI = 'school_manager/message';

    public static function update(int $id, array $data)
    {
        throw new InvalidMethodException(static::class, 'update');
    }

}
