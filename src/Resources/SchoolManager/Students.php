<?php

namespace Homeworker\Api\Resources\SchoolManager;

use Homeworker\Api\Client;
use Homeworker\Api\Resources\AbstractResource;
use Homeworker\Api\ResourceTraits\CollectionResource;
use Homeworker\Api\ResourceTraits\RetrievableResource;

class Students extends AbstractResource
{
    use RetrievableResource;
    use CollectionResource;

    const URI = 'school_manager/students';

    /** @var int */
    public $id;

    /** @var string */
    public $first_name;

    /** @var string */
    public $last_name;

    /** @var null|string */
    public $birthday;

    /** @var null|string */
    public $grade;

    /** @var null|string */
    public $course;

    /** @var null|string */
    public $parent_first_name;

    /** @var null|string */
    public $parent_last_name;

    /** @var null|string */
    public $parent_birthdate;

    /** @var null|string */
    public $parent_accepted_at;

    /** @var null|string */
    public $registration_key;

    /** @var null|string */
    public $registration_url;

    /** @var null|int */
    public $user_id;

    /** @var int */
    public $blocked;

    /** @var null|string */
    public $last_sync;

    /** @var null|string */
    public $import_source;

    /** @var null|string */
    public $import_key;

    /** @var bool */
    public $is_advocate;

    /**
     * Startet die Synchronisation der Daten für einen Schüler
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Homeworker\Api\Exceptions\InvalidApiTokenException
     * @throws \Homeworker\Api\Exceptions\MissingApiTokenException
     * @throws \Homeworker\Api\Exceptions\RequestException
     * @throws \Homeworker\Api\Exceptions\ResponseException
     */
    public function synchronize()
    {
        $response = Client::getRequestor()->sendPlain('POST', self::URI.'/'.$this->id.'/synchronize', []);

        return json_decode($response->getBody());
    }

    /**
     * Generiert ein neuen Anmelde-Schlüssel für einen Schüler
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Homeworker\Api\Exceptions\InvalidApiTokenException
     * @throws \Homeworker\Api\Exceptions\MissingApiTokenException
     * @throws \Homeworker\Api\Exceptions\RequestException
     * @throws \Homeworker\Api\Exceptions\ResponseException
     */
    public function reRollRegistrationKey()
    {
        $response = Client::getRequestor()->sendPlain('POST', self::URI.'/'.$this->id.'/re_roll/registration_key', []);

        return json_decode($response->getBody());
    }

    /**
     * Entfernt den aktuell verbundenen Nutzer vom Schüler
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Homeworker\Api\Exceptions\InvalidApiTokenException
     * @throws \Homeworker\Api\Exceptions\MissingApiTokenException
     * @throws \Homeworker\Api\Exceptions\RequestException
     * @throws \Homeworker\Api\Exceptions\ResponseException
     */
    public function disconnect()
    {
        $response = Client::getRequestor()->sendPlain('POST', self::URI.'/'.$this->id.'/disconnect', []);

        return json_decode($response->getBody());
    }

    /**
     * Sperrt einen Schüler
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Homeworker\Api\Exceptions\InvalidApiTokenException
     * @throws \Homeworker\Api\Exceptions\MissingApiTokenException
     * @throws \Homeworker\Api\Exceptions\RequestException
     * @throws \Homeworker\Api\Exceptions\ResponseException
     */
    public function block()
    {
        $response = Client::getRequestor()->sendPlain('POST', self::URI.'/'.$this->id.'/block', []);

        return json_decode($response->getBody());
    }

    /**
     * Entsperrt einen Schüler
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Homeworker\Api\Exceptions\InvalidApiTokenException
     * @throws \Homeworker\Api\Exceptions\MissingApiTokenException
     * @throws \Homeworker\Api\Exceptions\RequestException
     * @throws \Homeworker\Api\Exceptions\ResponseException
     */
    public function unblock()
    {
        $response = Client::getRequestor()->sendPlain('POST', self::URI.'/'.$this->id.'/unblock', []);

        return json_decode($response->getBody());
    }

    /**
     * Erhebt einen Schüler zum Botschafter
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Homeworker\Api\Exceptions\InvalidApiTokenException
     * @throws \Homeworker\Api\Exceptions\MissingApiTokenException
     * @throws \Homeworker\Api\Exceptions\RequestException
     * @throws \Homeworker\Api\Exceptions\ResponseException
     */
    public function promoteAsAdvocate()
    {
        $response = Client::getRequestor()->sendPlain('POST', self::URI.'/'.$this->id.'/advocate/promote', []);

        return json_decode($response->getBody());
    }

    /**
     * Erniedrigt einen Schüler von der Botschafter Position
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Homeworker\Api\Exceptions\InvalidApiTokenException
     * @throws \Homeworker\Api\Exceptions\MissingApiTokenException
     * @throws \Homeworker\Api\Exceptions\RequestException
     * @throws \Homeworker\Api\Exceptions\ResponseException
     */
    public function demoteAsAdvocate()
    {
        $response = Client::getRequestor()->sendPlain('POST', self::URI.'/'.$this->id.'/advocate/demote', []);

        return json_decode($response->getBody());
    }

    /**
     * Gibt den verbundenen Account zurück, wenn einer gefunden wurde
     *
     * @param string|null $username
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Homeworker\Api\Exceptions\InvalidApiTokenException
     * @throws \Homeworker\Api\Exceptions\MissingApiTokenException
     * @throws \Homeworker\Api\Exceptions\RequestException
     * @throws \Homeworker\Api\Exceptions\ResponseException
     */
    public function account(?string $username = null)
    {
        $response = Client::getRequestor()->sendPlain('GET', self::URI.'/'.$this->id.'/account', ['username' => $username]);

        $account = json_decode($response->getBody());
        if(empty((array) $account)) return null;

        return $account;
    }

    /**
     * Setzt das Passwort eines Benutzers auf sein Geburtsdatum zurück.
     *
     * @param string $format date() valides Datumsformat
     * @param string|null $admin_name Name des Benutzers, der das Passwort zurück gesetzt hat
     * @return bool Erfolgreich oder nicht
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Homeworker\Api\Exceptions\InvalidApiTokenException
     * @throws \Homeworker\Api\Exceptions\MissingApiTokenException
     * @throws \Homeworker\Api\Exceptions\RequestException
     * @throws \Homeworker\Api\Exceptions\ResponseException
     */
    public function resetPassword(string $format = 'Y-m-d', ?string $admin_name = null): bool
    {
        $response = Client::getRequestor()->sendPlain('POST', self::URI.'/'.$this->id.'/account/reset_password', [
            'format' => $format,
            'admin_name' => $admin_name
        ]);

        $reset = json_decode($response->getBody(), true);

        return isset($reset['success']) && $reset['success'];
    }

    /**
     * Gibt die Gruppen zurück, in welchen der Schüler Mitglied ist.
     *
     * @return Groups[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Homeworker\Api\Exceptions\InvalidApiTokenException
     * @throws \Homeworker\Api\Exceptions\MissingApiTokenException
     * @throws \Homeworker\Api\Exceptions\RequestException
     * @throws \Homeworker\Api\Exceptions\ResponseException
     */
    public function groups()
    {
        $response = Client::getRequestor()->sendPlain('GET', self::URI.'/'.$this->id.'/groups', []);

        $responseGroups = json_decode($response->getBody(), true);
        $groups = [];
        foreach ($responseGroups as $responseGroup) {
            $groups[] = Groups::__set_state($responseGroup);
        }

        return $groups;
    }

    /**
     * Gibt den ganzen Namen eines Schülers zurück
     *
     * @param bool $reversed
     * @return string
     */
    public function getFullName(bool $reversed = false): string
    {
        if ($reversed) return $this->last_name.', '.$this->first_name;

        return $this->first_name.' '.$this->last_name;
    }

}
