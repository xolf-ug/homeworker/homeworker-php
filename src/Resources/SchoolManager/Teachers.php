<?php

namespace Homeworker\Api\Resources\SchoolManager;

use Homeworker\Api\Client;
use Homeworker\Api\Resources\AbstractResource;
use Homeworker\Api\ResourceTraits\CollectionResource;
use Homeworker\Api\ResourceTraits\RetrievableResource;

class Teachers extends AbstractResource
{
    use RetrievableResource;
    use CollectionResource;

    const URI = 'school_manager/teachers';

    /** @var int */
    public $id;

    /** @var string */
    public $first_name;

    /** @var string */
    public $last_name;

    /** @var string|null */
    public $gender;

    /** @var string */
    public $use_url;

    /** @var null|string */
    public $import_source;

    /** @var null|string */
    public $import_key;

    /**
     * Gibt den ganzen Namen eines Schülers zurück
     *
     * @param bool $reversed
     * @return string
     */
    public function getFullName(bool $reversed = false): string
    {
        if ($reversed) return $this->last_name.', '.$this->first_name;

        return $this->first_name.' '.$this->last_name;
    }

    /**
     * Gibt alle Chats, in welchen der Lehrer Mitglied ist, zurück.
     *
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Homeworker\Api\Exceptions\InvalidApiTokenException
     * @throws \Homeworker\Api\Exceptions\MissingApiTokenException
     * @throws \Homeworker\Api\Exceptions\RequestException
     * @throws \Homeworker\Api\Exceptions\ResponseException
     */
    public function chats()
    {
        $response = Client::getRequestor()->sendPlain('GET', self::URI.'/'.$this->id.'/chats', []);

        return json_decode($response->getBody(), true);
    }

}
