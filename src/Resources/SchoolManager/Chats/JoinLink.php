<?php

namespace Homeworker\Api\Resources\SchoolManager\Chats;

use Homeworker\Api\Client;
use Homeworker\Api\Resources\AbstractResource;

class JoinLink extends AbstractResource
{

    /**
     * Get Join Link for a Chat-Room
     *
     * @param int $chat_raum_id
     * @param string $teacher_name
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Homeworker\Api\Exceptions\InvalidApiTokenException
     * @throws \Homeworker\Api\Exceptions\MissingApiTokenException
     * @throws \Homeworker\Api\Exceptions\RequestException
     * @throws \Homeworker\Api\Exceptions\ResponseException
     */
    public static function get(int $chat_raum_id, string $teacher_name)
    {
        $response = Client::getRequestor()->sendPlain("GET", 'school_manager/chats/'.$chat_raum_id.'/classroom/join_link', ['teacher_name' => $teacher_name]);

        return json_decode($response->getBody(), true)['link'];
    }

}
