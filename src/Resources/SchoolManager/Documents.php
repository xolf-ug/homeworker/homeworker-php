<?php

namespace Homeworker\Api\Resources\SchoolManager;

use Homeworker\Api\Resources\AbstractResource;
use Homeworker\Api\ResourceTraits\CollectionResource;
use Homeworker\Api\ResourceTraits\DeletableResource;
use Homeworker\Api\ResourceTraits\RetrievableResource;
use Homeworker\Api\ResourceTraits\UploadableResource;

class Documents extends AbstractResource
{
    use RetrievableResource;
    use CollectionResource;
    use DeletableResource;
    use UploadableResource;

    const URI = 'school_manager/documents';

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $hash;

    /** @var string */
    public $sha1_checksum;

    /** @var string */
    public $content_type;

    /** @var int */
    public $size;

    /** @var null|string */
    public $related_to;

    /** @var string */
    public $uploaded;

    /** @var string */
    public $url_by_hash;

    /** @var string */
    public $download_url;

    /** @var bool */
    public $can_delete;

    /** @var string */
    public $human_size;

    /** @var string */
    public $type;


}
