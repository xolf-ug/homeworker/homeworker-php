<?php

namespace Homeworker\Api\Resources\SchoolManager;

use Homeworker\Api\Client;
use Homeworker\Api\Resources\AbstractResource;
use Homeworker\Api\ResourceTraits\RetrievableResource;

class School extends AbstractResource
{
    use RetrievableResource;

    const URI = 'school_manager/school';

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var null|string */
    public $address;

    /** @var null|float */
    public $lat;

    /** @var null|float */
    public $lon;

    /** @var null|string */
    public $website;

    /** @var null|string */
    public $logo;

    /** @var null|string */
    public $phone;

    /** @var null|string */
    public $director;

    /** @var null|string */
    public $region;

    /** @var null|string */
    public $city;

    /** @var int */
    public $admin_user_id;

    /**
     * Gibt die Schüler zurück, welche als Botschafter eingetragen sind.
     *
     * @return Students[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Homeworker\Api\Exceptions\InvalidApiTokenException
     * @throws \Homeworker\Api\Exceptions\MissingApiTokenException
     * @throws \Homeworker\Api\Exceptions\RequestException
     * @throws \Homeworker\Api\Exceptions\ResponseException
     */
    public function advocates()
    {
        $response = Client::getRequestor()->sendPlain('GET', self::URI.'/advocates', []);

        $responseAdvocates = json_decode($response->getBody(), true);
        $students = [];
        foreach ($responseAdvocates as $responseAdvocate) {
            $students[] = Students::__set_state($responseAdvocate);
        }

        return $students;
    }

}
