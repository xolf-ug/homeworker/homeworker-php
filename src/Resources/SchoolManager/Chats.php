<?php

namespace Homeworker\Api\Resources\SchoolManager;

use Homeworker\Api\Client;
use Homeworker\Api\Resources\AbstractResource;
use Homeworker\Api\ResourceTraits\CollectionResource;
use Homeworker\Api\ResourceTraits\DeletableResource;
use Homeworker\Api\ResourceTraits\RetrievableResource;

class Chats extends AbstractResource
{
    use RetrievableResource;
    use CollectionResource;
    use DeletableResource;

    const URI = 'school_manager/chats';

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $created_at;

    /** @var null|string */
    public $last_message_at;

    /** @var bool */
    public $broadcast_only;

    /** @var bool */
    public $files_allowed;

    /** @var bool */
    public $everybody_can_start_call;

    /** @var null|string */
    public $call_open_until;

    /** @var string */
    public $url;

    /** @var bool|null */
    public $is_admin;

    /** @var bool|null */
    public $can_write;

    /** @var bool|null */
    public $can_start_call;

    /** @var bool */
    public $call_is_open;

    /** @var int|null */
    public $unred_messages;

    /** @var bool|null */
    public $is_muted;

    /** @var bool|null */
    public $is_pinned;

    /** @var object|null */
    public $last_message;

}
