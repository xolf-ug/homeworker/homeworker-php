<?php

namespace Homeworker\Api\Resources\SchoolManager;

use Homeworker\Api\Client;
use Homeworker\Api\Resources\AbstractResource;
use Homeworker\Api\ResourceTraits\CollectionResource;
use Homeworker\Api\ResourceTraits\RetrievableResource;

class Groups extends AbstractResource
{
    use RetrievableResource;
    use CollectionResource;

    const URI = 'school_manager/groups';

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /**
     * Gibt alle Mitglieder der Gruppe zurück
     *
     * @return Students[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Homeworker\Api\Exceptions\InvalidApiTokenException
     * @throws \Homeworker\Api\Exceptions\MissingApiTokenException
     * @throws \Homeworker\Api\Exceptions\RequestException
     * @throws \Homeworker\Api\Exceptions\ResponseException
     */
    public function members()
    {
        $response = Client::getRequestor()->sendPlain('GET', self::URI.'/'.$this->id.'/members');

        $responseStudents = json_decode($response->getBody(), true);
        $students = [];
        foreach ($responseStudents as $responseStudent) {
            $students[] = Students::__set_state($responseStudent);
        }

        return $students;
    }

}
