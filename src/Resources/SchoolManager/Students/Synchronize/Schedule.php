<?php

namespace Homeworker\Api\Resources\SchoolManager\Students\Synchronize;

use Homeworker\Api\Resources\AbstractResource;
use Homeworker\Api\ResourceTraits\RetrievableResource;

class Schedule extends AbstractResource
{
    use RetrievableResource;

    public $timetable;

    public const URI = "school_manager/students/synchronize/schedule";

}
