<?php

namespace Homeworker\Api\Resources\SchoolManager\Students;

use Homeworker\Api\Resources\AbstractResource;
use Homeworker\Api\Resources\SchoolManager\Students\Import\ImportRequest;
use Homeworker\Api\ResourceTraits\EditableResource;

class Import extends AbstractResource
{
    use EditableResource;

    public const URI = "school_manager/students/import";

    /**
     * @param ImportRequest $importRequest
     * @return Import
     */
    public static function import(ImportRequest $importRequest): Import
    {
        return self::create($importRequest->toArray());
    }

}
