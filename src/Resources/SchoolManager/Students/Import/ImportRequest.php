<?php

namespace Homeworker\Api\Resources\SchoolManager\Students\Import;

use Homeworker\Api\Traits\ArraybleTrait;

class ImportRequest
{
    use ArraybleTrait;

    /** @var ImportStudent[] */
    public $students;

    /** @var string */
    public $source;

    const SOURCE_API = 'api';
    const SOURCE_INFOPORTAL = 'infoportal';

    /** @var string[] */
    public $overwrite_rule = [];
    const OVERWRITE_RULE_CLEAR = 'clear';
    const OVERWRITE_RULE_OVERWRITE = 'overwrite';
    const OVERWRITE_RULE_APPEND = 'append';
    const OVERWRITE_DELETE_NON_MATCHTED = 'delete_non_matched';

}
