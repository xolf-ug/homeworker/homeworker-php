<?php

namespace Homeworker\Api\Resources\SchoolManager\Students\Import;

use Homeworker\Api\Traits\ArraybleTrait;
use Homeworker\Api\Traits\SetStatableTrait;

class ImportStudent
{
    use ArraybleTrait, SetStatableTrait;

    /** @var string */
    public $first_name;

    /** @var string */
    public $last_name;

    /** @var string */
    public $birthday;

    /** @var string */
    public $grade;

    /** @var string */
    public $course;

    /** @var string */
    public $import_key;

}
