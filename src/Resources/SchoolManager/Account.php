<?php

namespace Homeworker\Api\Resources\SchoolManager;

use Homeworker\Api\Resources\AbstractResource;
use Homeworker\Api\ResourceTraits\RetrievableResource;

class Account extends AbstractResource
{
    use RetrievableResource;

    const URI = 'school_manager/account';

}
