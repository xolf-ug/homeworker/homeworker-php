<?php

namespace Homeworker\Api\Resources;

use Homeworker\Api\Traits\ArraybleTrait;
use Homeworker\Api\Traits\SetStatableTrait;

abstract class AbstractResource
{
    use SetStatableTrait;
    use ArraybleTrait;


    const URI = "";

}
