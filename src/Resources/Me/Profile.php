<?php

namespace Homeworker\Api\Resources\Me;

use Homeworker\Api\Resources\AbstractResource;
use Homeworker\Api\ResourceTraits\RetrievableResource;

class Profile extends AbstractResource
{
    use RetrievableResource;

    public const URI = "me/profile";

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string|null */
    public $mail;

    /** @var bool|null */
    public $mail_verified;

    /** @var string|null */
    public $mobile;

    /** @var bool|null */
    public $mobile_verified;

}
