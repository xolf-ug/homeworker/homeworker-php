<?php

namespace Homeworker\Api\Resources;

use Homeworker\Api\ResourceTraits\RetrievableResource;

class Ips extends AbstractResource
{
    use RetrievableResource;

    public const URI = "ips";

}
