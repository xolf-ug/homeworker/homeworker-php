<?php

namespace Homeworker\Api\Resources\Exceptions;

use Homeworker\Api\Exceptions\HomeworkerException;

class ResourceException extends HomeworkerException {}
