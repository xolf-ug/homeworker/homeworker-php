<?php

namespace Homeworker\Api\Resources\Exceptions;

use Throwable;

class InvalidMethodException extends ResourceException
{

    public function __construct(string $resourceClass, string $method, Throwable $previous = null)
    {
        $message = $method." method is not supported on ".$resourceClass;

        parent::__construct($message, 0, $previous);
    }

}
