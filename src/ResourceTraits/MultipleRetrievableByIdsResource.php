<?php

namespace Homeworker\Api\ResourceTraits;

use Homeworker\Api\Client;

/**
 * Is a resource retrievable, e.g. allows GET request
 *
 * Trait RetrievableResource
 * @package Homeworker\Api\Resources\Traits
 */
trait MultipleRetrievableByIdsResource
{

    /**
     * Retrieve resources by their IDs
     *
     * @param int[] $ids
     * @return static
     */
    public static function retrieveByIds(array $ids)
    {
        $filledResource = Client::getRequestor()->send("GET", static::class, null, ['ids' => $ids]);

        return $filledResource;
    }

}