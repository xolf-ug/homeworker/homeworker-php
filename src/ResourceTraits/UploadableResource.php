<?php

namespace Homeworker\Api\ResourceTraits;

use GuzzleHttp\RequestOptions;
use Homeworker\Api\Client;
use Homeworker\Api\Resources\AbstractResource;

/**
 * Is a resource a collection, e.g. there are multiple objects on a path
 *
 * Trait CollectionResource
 * @package Homeworker\Api\Resources\Traits
 */
trait UploadableResource
{

    /**
     * Upload files as documents.
     *
     * @param array $files Multipart conform array of files. See http://docs.guzzlephp.org/en/stable/request-options.html#multipart
     * @return static[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Homeworker\Api\Exceptions\InvalidApiTokenException
     * @throws \Homeworker\Api\Exceptions\MissingApiTokenException
     * @throws \Homeworker\Api\Exceptions\RequestException
     * @throws \Homeworker\Api\Exceptions\ResponseException
     */
    public static function upload(array $files)
    {
        $response = Client::getRequestor()->sendPlain("POST", static::URI, [], [
            RequestOptions::MULTIPART => $files,
        ]);

        $allResources = [];
        $className = static::class;

        $jsonResponse = json_decode($response->getBody(), true);
        foreach ($jsonResponse as $jsonElement) {
            /** @var AbstractResource $resource */
            $resource = new $className();

            /** @var AbstractResource $instance */
            $allResources[] = $resource::__set_state($jsonElement);
        }

        return $allResources;
    }

}