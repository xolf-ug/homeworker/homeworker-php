<?php

namespace Homeworker\Api\ResourceTraits;

use Homeworker\Api\Client;

/**
 * Is a resource retrievable, e.g. allows GET request
 *
 * Trait RetrievableResource
 * @package Homeworker\Api\Resources\Traits
 */
trait RetrievableResource
{
    use MultipleRetrievableByIdsResource;

    /**
     * Retrieve a resource
     *
     * @param int|null $id
     * @return static
     */
    public static function retrieve(?int $id = null)
    {
        $filledResource = Client::getRequestor()->send("GET", static::class, $id);

        return $filledResource;
    }

}