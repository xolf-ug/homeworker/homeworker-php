<?php

namespace Homeworker\Api\ResourceTraits;

use Homeworker\Api\Client;
use Homeworker\Api\Resources\AbstractResource;

/**
 * Is a resource a collection, e.g. there are multiple objects on a path
 *
 * Trait CollectionResource
 * @package Homeworker\Api\Resources\Traits
 */
trait CollectionResource
{

    /**
     * Returns all resources
     *
     * @param array $filters
     * @return static[]
     */
    public static function all(array $filters = []): array
    {
        $response = Client::getRequestor()->sendPlain("GET", static::URI, $filters);

        $allResources = [];
        $className = static::class;

        $jsonResponse = json_decode($response->getBody(), true);
        foreach ($jsonResponse as $jsonElement) {
            /** @var AbstractResource $resource */
            $resource = new $className();

            /** @var AbstractResource $instance */
            $allResources[] = $resource::__set_state($jsonElement);
        }

        return $allResources;
    }

}