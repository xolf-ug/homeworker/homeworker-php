<?php

namespace Homeworker\Api\ResourceTraits;

use Homeworker\Api\Client;

/**
 * Is a resource deletable, e.g. allows DELETE method
 *
 * Trait DeletableResource
 * @package Homeworker\Api\Resources\Traits
 */
trait DeletableResource
{

    /**
     * Deletes the current resource
     */
    public function delete()
    {
        Client::getRequestor()->send("DELETE", static::class, $this->id);
    }

}