<?php

namespace Homeworker\Api\ResourceTraits;

use Homeworker\Api\Client;

/**
 * Is a resource editable, e.g. allows POST requests
 *
 * Trait EditableResource
 * @package Homeworker\Api\Resources\Traits
 */
trait EditableResource
{

    /**
     * Create a resource
     *
     * @param array $data
     * @return static
     */
    public static function create(array $data)
    {
        $createdResource = Client::getRequestor()->send("POST", static::class, null, $data);

        return $createdResource;
    }

    /**
     * Update a resource
     *
     * @param int $id
     * @param array $datay
     * @return static
     */
    public static function update(int $id, array $data)
    {
        $updatedResource = Client::getRequestor()->send("POST", static::class, $id, $data);

        return $updatedResource;
    }

    /**
     * Saves the current resource
     */
    public function save()
    {
        if(isset($this->id) && is_int($this->id)) {
            self::update($this->id, $this->toArray());
        } else {
            self::create($this->toArray());
        }

    }

}